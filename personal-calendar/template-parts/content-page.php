<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content">
		<?php
		if ( is_user_logged_in() ) {
			the_content(); ?>
			<div>
				&nbsp;&nbsp;&nbsp;<a class="btn" href="<?php echo admin_url( '/index.php' ); ?>"><?php _e( 'Dashboard' );?></a>
				&nbsp;&nbsp;&nbsp;<a class="btn" href="<?php echo wp_logout_url( get_bloginfo('url') ); ?>"><?php _e( 'Log out' );?></a>
			</div>
			<?php
		} else {
			$args = array(
				'echo'           => true,
				'remember'       => true,
				'redirect'       => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
				'form_id'        => 'loginform',
				'id_username'    => 'user_login',
				'id_password'    => 'user_pass',
				'id_remember'    => 'rememberme',
				'id_submit'      => 'wp-submit',
				'label_username' => __( 'Username' ),
				'label_password' => __( 'Password' ),
				'label_remember' => __( 'Remember Me' ),
				'label_log_in'   => __( 'Log In' ),
				'value_username' => '',
				'value_remember' => true
			);
			wp_login_form( $args );
		} ?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
